import * as ELEMENTS from './elements';
import Http from './http';
import { WeatherData , WEATHER_PROXY_HANDLER} from './weather-data';

ELEMENTS.ELEMENT_SEARCH_BUTTON.addEventListener('click', () => {
    
    const CITY_NAME = ELEMENTS.ELEMENT_SEARCHED_CITY.value.trim();
    const KEY = '76f9dade6c9b31e36181ad679bfa630e';
    const URL = `http://api.openweathermap.org/data/2.5/weather?q=${CITY_NAME}&units=metric&appid=${KEY}`;
    
    if(CITY_NAME.length !== 0){
        ELEMENTS.ELEMENT_LOADING.style = 'display: block';
        Http.fetchData(URL)
            .then(data => {
                const WEATHER_DATA = new WeatherData(CITY_NAME, data.weather[0].description.toUpperCase(), data.main.temp);
                const WEATHER_PROXY = new Proxy(WEATHER_DATA, WEATHER_PROXY_HANDLER);
                WEATHER_PROXY.temperature = data.main.temp;
                updateWeather(WEATHER_DATA);
            })
            .catch(error => {
                ELEMENTS.ELEMENT_LOADING.style = 'display: none';
                alert(error);
            });
    }else{
        ELEMENTS.ELEMENT_LOADING.style = 'display: none';
        ELEMENTS.ELEMENT_WEATHER_DIV.style = 'display: none';
        alert("Please enter the city name.")
    }

    function updateWeather(data){
        console.log(data);
        ELEMENTS.ELEMENT_LOADING.style = 'display: none';
        ELEMENTS.ELEMENT_WEATHER_DIV.style = 'display: block';
        ELEMENTS.ELEMENT_WEATHER_CITY.textContent = data.cityName;
        ELEMENTS.ELEMENT_WEATHER_DESC.textContent = data.description;
        ELEMENTS.ELEMENT_WEATHER_TEMP.textContent = data.temperatureC;


    }

        
    
});