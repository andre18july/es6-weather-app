class Http{
    static fetchData(url){       
        return new Promise((resolve, reject) => {
            //fecthdata
            const HTTP = new XMLHttpRequest();
            const METHOD = 'GET';

            HTTP.open(METHOD, url);

            HTTP.onreadystatechange = () => {
                if(HTTP.readyState == XMLHttpRequest.DONE && HTTP.status === 200){
                    const DATA = JSON.parse(HTTP.responseText);
                    resolve(DATA);
                }else if(HTTP.readyState == XMLHttpRequest.DONE){
                    reject('An error has occurred!');
                }
            }

            HTTP.send();
            
        })    
    }
}

export default Http;