export class WeatherData{
    constructor(cityName, description, temperatureC){
        this.cityName = cityName;
        this.description = description;
        this.temperatureC = temperatureC;
        this.temperature = "";
    }
}


export const WEATHER_PROXY_HANDLER = {
    get: function(target, propertry){
        return Reflect.get(target, propertry);
    },
    set: function(target, propertry, value){
        const newValue = (value * 1.8 + 32).toFixed(2) + 'F.';
        return Reflect.set(target, propertry, newValue);
    }
};